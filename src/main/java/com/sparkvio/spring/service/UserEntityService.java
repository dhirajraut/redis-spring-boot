package com.sparkvio.spring.service;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.google.common.reflect.TypeToken;
import com.sparkvio.spring.entity.UserEntity;
import com.sparkvio.spring.exception.EntityNotFoundException;
import com.sparkvio.spring.exception.IntegrityViolationException;
import com.sparkvio.spring.jpa.IUserEntityRepository;
import com.sparkvio.spring.model.UserVO;

@Service
public class UserEntityService implements IEntityService<UserVO> {

	@Autowired
	IUserEntityRepository repository;
	@Autowired
	ModelMapper modelMapper;

	@Override
	public Iterable<UserVO> findAll() {
		List<UserEntity> userEntityList = repository.findAll(Sort.by(Direction.ASC, "firstName"));
		List<UserVO> userVOList = modelMapper.map(userEntityList, new TypeToken<List<UserVO>>() {
		}.getType());
		return userVOList;
	}

	@Override
	public Iterable<UserVO> saveAll(Iterable<UserVO> sampleVOList)
			throws IntegrityViolationException, EntityNotFoundException {
		Iterable<UserEntity> UserEntityList = modelMapper.map(sampleVOList, new TypeToken<List<UserEntity>>() {
		}.getType());

		try {
			Iterable<UserEntity> savedEntities = repository.saveAll(UserEntityList);
			Iterable<UserVO> savedUserVOList = modelMapper.map(savedEntities, new TypeToken<List<UserVO>>() {
			}.getType());
			return savedUserVOList;
		} catch (DataIntegrityViolationException exception) {
			throw new IntegrityViolationException("Could not save object(s)", exception);
		}
	}

	@Override
	public void deleteById(Long id) throws EntityNotFoundException {
		if (repository.existsById(id)) {
			repository.deleteById(id);
		} else {
			throw new EntityNotFoundException("Couldn't find entity with id: " + id);
		}
	}

	@Override
	public UserVO findById(Long id) throws EntityNotFoundException {
		UserEntity userEntity = repository.findById(id)
				.orElseThrow(() -> new EntityNotFoundException("Couldn't find entity with id: " + id));
		UserVO sampleVO = modelMapper.map(userEntity, UserVO.class);
		return sampleVO;
	}

}
