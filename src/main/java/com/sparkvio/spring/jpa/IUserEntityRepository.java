package com.sparkvio.spring.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sparkvio.spring.entity.UserEntity;

/**
 * JPA Repository for entity.
 */
public interface IUserEntityRepository extends JpaRepository<UserEntity, Long> {

}
