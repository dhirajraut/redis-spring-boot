package com.sparkvio.spring.controller;

public interface RestConstants {

	public static final String URL_USERS_BASE = "/users";
	public static final String URL_USERS_BYID = "/{id}";
}
