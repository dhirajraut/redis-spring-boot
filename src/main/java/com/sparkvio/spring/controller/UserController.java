package com.sparkvio.spring.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sparkvio.spring.model.UserVO;
import com.sparkvio.spring.service.IEntityService;

@RestController
@RequestMapping(path = "v1/" + RestConstants.URL_USERS_BASE)
public class UserController implements IEntityController<UserVO> {

	@Autowired
	IEntityService<UserVO> userEntityService;

	@RequestMapping(method = RequestMethod.GET, path = RestConstants.URL_USERS_BYID)
	@Cacheable (value = "users", key = "#id")
	@Override
	public UserVO findById(/* Variable in URL path */ @PathVariable Long id) {
		return userEntityService.findById(id);
	}

	@RequestMapping(method = RequestMethod.GET)
	@Override
	public Iterable<UserVO> findAll() {
		return userEntityService.findAll();
	}

	@RequestMapping(method = RequestMethod.POST)
	@CachePut (value = "users", key = "#userVO.id")
	@ResponseStatus(HttpStatus.CREATED)
	@Override
	public Iterable<UserVO> save(UserVO userVO) {
		List<UserVO> UserVOList = new ArrayList<UserVO>();
		UserVOList.add(userVO);
		return userEntityService.saveAll(UserVOList);
	}

	// @RequestMapping(method = RequestMethod.POST)
	// @ResponseStatus(HttpStatus.CREATED)
	// @Override
	// @TODO How to add list of new objects?
	public Iterable<UserVO> saveAll(Iterable<UserVO> UserVOList) {
		return userEntityService.saveAll(UserVOList);
	}

	@RequestMapping(method = RequestMethod.PUT, path = RestConstants.URL_USERS_BYID)
	@ResponseStatus(HttpStatus.OK)
	@Override
	public Iterable<UserVO> update(/* Variable in URL Path */ @PathVariable Long id, UserVO userVO) {
		userVO.setId(id);
		List<UserVO> userVOList = new ArrayList<UserVO>();
		userVOList.add(userVO);
		return userEntityService.saveAll(userVOList);
	}

	// @TODO How to patch the objects?
	@RequestMapping(method = RequestMethod.PATCH, path = RestConstants.URL_USERS_BYID)
	@ResponseStatus(HttpStatus.OK)
	public Iterable<UserVO> partialUpdate(/* Variable in URL Path */ @PathVariable Long id, UserVO userVO) {
		userVO.setId(id);
		List<UserVO> userVOList = new ArrayList<UserVO>();
		userVOList.add(userVO);
		return userEntityService.saveAll(userVOList);
	}

	@RequestMapping(method = RequestMethod.DELETE, path = RestConstants.URL_USERS_BYID)
	@CacheEvict (value = "users", key = "#id")
	@Override
	public void deleteById(/* Variable in URL path */ @PathVariable Long id) {
		userEntityService.deleteById(id);
	}


}
