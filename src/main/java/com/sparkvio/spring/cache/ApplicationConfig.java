package com.sparkvio.spring.cache;

import org.modelmapper.ModelMapper;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan(basePackages = "com.sparkvio.spring")
@EntityScan(basePackages = "com.sparkvio.spring.entity")
@EnableJpaRepositories(basePackages = "com.sparkvio.spring.jpa")
@EnableCaching
@Configuration
public class ApplicationConfig {

	/**
	 * Create ModelMapper bean.
	 * 
	 * @return
	 */
	@Bean
	public ModelMapper modelMapper() {
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper;
	}

}
